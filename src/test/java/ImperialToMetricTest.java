
import converter.ImperialToMetric;
import exception.LessThanZeroException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class ImperialToMetricTest {

    private ImperialToMetric imperialToMetric = new ImperialToMetric();

    private final Double FOOT_METER = 0.3048;
    private final Double INCH_CENTIMETER = 2.54;
    private final Double GALLON_LITER = 3.7854;
    private final Double POUND_KILOGRAM = 0.4536;

    @BeforeAll
    public static void setUpAll() {
        System.out.println("------ Convert imperial unit to metric unit Test -----");
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectFoot_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            imperialToMetric.footToMeter(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectInch_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            imperialToMetric.inchToCentimeter(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectGallon_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            imperialToMetric.gallonToLiter(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectPound_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            imperialToMetric.poundToKilogram(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenNegativeFahrenheit_shouldReturnCorrectCelsius(Double number) {

        Double expectedValue = (number - 32.0) * (5.0 / 9.0);
        Double result = imperialToMetric.fahrenheitToCelsius(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectFoot_shouldReturnCorrectMeter (Double number){
        Double expectedValue = number * FOOT_METER;
        Double result = imperialToMetric.footToMeter(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectInch_shouldReturnCorrectCentimeter (Double number){
        Double expectedValue = number * INCH_CENTIMETER;
        Double result = imperialToMetric.inchToCentimeter(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectGallon_shouldReturnCorrectLiter (Double number){
        Double expectedValue = number * GALLON_LITER;
        Double result = imperialToMetric.gallonToLiter(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectPound_shouldReturnCorrectKilogram (Double number){
        Double expectedValue = number * POUND_KILOGRAM;
        Double result = imperialToMetric.poundToKilogram(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectFahrenheit_shouldReturnCorrectCelsius (Double number){
        Double expectedValue =(number - 32.0) * (5.0 / 9.0);
        Double result = imperialToMetric.fahrenheitToCelsius(number);
        Assertions.assertEquals(expectedValue, result);
    }

    private static Stream numbersLessThanZero() {

        return Stream.of(
                Arguments.of(-1d),
                Arguments.of(-3.5),
                Arguments.of(-0.25)
        );
    }
    private static Stream correctNumbers() {

        return Stream.of(
                Arguments.of(0d),
                Arguments.of(2d),
                Arguments.of(100d),
                Arguments.of(1.0),
                Arguments.of(0.00005)
        );
    }
}

