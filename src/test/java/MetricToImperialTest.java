import converter.MetricToImperial;
import exception.LessThanZeroException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

public class MetricToImperialTest {

    private MetricToImperial metricToImperial = new MetricToImperial();

    private final Double METER_FOOT = 3.2808;
    private final Double CENTIMETER_INCH = 0.3937;
    private final Double LITER_GALLON = 0.2642;
    private final Double KILOGRAM_POUND = 2.2046;

    @BeforeAll
    public static void setUpAll() {
        System.out.println("------ Convert metric unit to imperial unit Test -----");
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectMeter_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            metricToImperial.meterToFoot(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectCentimeter_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            metricToImperial.centimeterToInch(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectLiter_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            metricToImperial.literToGallon(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenInorrectKilogram_shouldReturnThrowLessThanZeroException(Double number) {

        Assertions.assertThrows(LessThanZeroException.class, () -> {
            metricToImperial.kilogramToPound(number);
        });
    }

    @ParameterizedTest
    @MethodSource("numbersLessThanZero")
    void givenNegativeCelsius_shouldReturnCorrectFahrenheit(Double number) {

        Double expectedValue = number * 9.0 / 5.0 + 32.0;
        Double result = metricToImperial.celsiusToFahrenheit(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectMeter_shouldReturnCorrectFoot (Double number){
        Double expectedValue = number * METER_FOOT;
        Double result = metricToImperial.meterToFoot(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectCentimeter_shouldReturnCorrectInch (Double number){
        Double expectedValue = number * CENTIMETER_INCH;
        Double result = metricToImperial.centimeterToInch(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectLiter_shouldReturnCorrectGallon (Double number){
        Double expectedValue = number * LITER_GALLON;
        Double result = metricToImperial.literToGallon(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectKilogram_shouldReturnCorrectPound (Double number){
        Double expectedValue = number * KILOGRAM_POUND;
        Double result = metricToImperial.kilogramToPound(number);
        Assertions.assertEquals(expectedValue, result);
    }

    @ParameterizedTest
    @MethodSource("correctNumbers")
    void givenCorrectCelsius_shouldReturnCorrectFahrenheit (Double number){
        Double expectedValue =number * 9.0 / 5.0 + 32.0;
        Double result = metricToImperial.celsiusToFahrenheit(number);
        Assertions.assertEquals(expectedValue, result);
    }

    private static Stream numbersLessThanZero() {

        return Stream.of(
                Arguments.of(-1d),
                Arguments.of(-3.5),
                Arguments.of(-0.25)
        );
    }
    private static Stream correctNumbers() {

        return Stream.of(
                Arguments.of(0d),
                Arguments.of(2d),
                Arguments.of(100d),
                Arguments.of(1.0),
                Arguments.of(0.00005)
        );
    }
}