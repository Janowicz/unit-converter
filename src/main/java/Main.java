import exception.LessThanZeroException;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, LessThanZeroException {

        MyConverter myConverter = new MyConverter();
        myConverter.run();


    }
}
