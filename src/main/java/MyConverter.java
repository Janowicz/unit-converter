import converter.ImperialToMetric;
import converter.MetricToImperial;
import exception.LessThanZeroException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MyConverter {

    private MetricToImperial metricToImperial = new MetricToImperial();
    private ImperialToMetric imperialToMetric = new ImperialToMetric();
    private final static String WRONG_CHOICE = "Your choice is not a number or decimal separator is wrong!";
    private final static String WRONG_NUMBER = "Select a correct number!";

    public void run() throws IOException, LessThanZeroException {

        while (true) {
            System.out.println(" ______________________________________");
            System.out.println("| Choose a type od conversion system.  |");
            System.out.println("|    1: metric -> imperial             |");
            System.out.println("|    2: imperial -> metric             |");
            System.out.println("|    3: exit                           |");
            System.out.println("|______________________________________|");

            String number = loadCharacters();
            mySwitch(number);
        }
    }

    private void mySwitch(String choose) throws IOException, LessThanZeroException {

        switch (choose) {
            case "1":
                metricToImperial.metricInterface();
                String firstCase = loadCharacters();
                metricToImperialSwitch(firstCase);
                break;
            case "2":
                imperialToMetric.imperialInterface();
                String secondCase = loadCharacters();
                imperialToMetricSwitch(secondCase);
                break;
            case "3":
                exit();
                break;
            default:
                System.out.println("Select number 1, 2 or 3!");
                break;
        }
    }

    private void exit() throws IOException {
        System.out.println("Do you want to close a program? [y/n]");
        String choose = loadCharacters();

        switch (choose) {
            case "y":
                System.out.println("The program is closing...");
                System.exit(0);
                break;
            case "n":
                System.out.println("The program continues...");
                break;
            default:
                System.out.println("You have chosen the wrong option!");
                break;
        }
    }

    public void metricToImperialSwitch(String firstCase) throws LessThanZeroException {
        switch (firstCase) {
            case "1":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double foot = metricToImperial.meterToFoot(chooseNumber);
                    System.out.println(foot + " foots");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "2":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double inch = metricToImperial.centimeterToInch(chooseNumber);
                    System.out.println(inch + " inches");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "3":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double gallon = metricToImperial.literToGallon(chooseNumber);
                    System.out.println(gallon + " gallons");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "4":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double pound = metricToImperial.kilogramToPound(chooseNumber);
                    System.out.println(pound + " pounds");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "5":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double fahrenheit = metricToImperial.celsiusToFahrenheit(chooseNumber);
                    System.out.println(fahrenheit + " degrees Fahrenheit");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            default:
                System.out.println(WRONG_NUMBER);
                break;
        }
    }

    public void imperialToMetricSwitch(String secondCase) throws LessThanZeroException {
        switch (secondCase) {
            case "1":
                System.out.println("Enter number to convert.");
                try {
                    Double number = loadCharactersDouble();
                    Double meter = imperialToMetric.footToMeter(number);
                    System.out.println(meter + " meters");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "2":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double centimeter = imperialToMetric.inchToCentimeter(chooseNumber);
                    System.out.println(centimeter + " centimeters");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "3":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double liter = imperialToMetric.gallonToLiter(chooseNumber);
                    System.out.println(liter + " liters");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "4":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double kilogram = imperialToMetric.poundToKilogram(chooseNumber);
                    System.out.println(kilogram + " kilograms");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (LessThanZeroException e) {
                    System.err.println(e.getMessage());
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            case "5":
                System.out.println("Enter number to convert.");
                try {
                    Double chooseNumber = loadCharactersDouble();
                    Double celsius = imperialToMetric.fahrenheitToCelsius(chooseNumber);
                    System.out.println(celsius + " degrees Celsius");
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    System.err.println(WRONG_CHOICE);
                }
                break;
            default:
                System.out.println(WRONG_NUMBER);
                break;
        }
    }

    private static String loadCharacters() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }

    public static Double loadCharactersDouble() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return Double.parseDouble(reader.readLine());
    }
}