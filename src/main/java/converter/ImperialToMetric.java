package converter;

import exception.LessThanZeroException;

public class ImperialToMetric {

    private final Double FOOT_METER = 0.3048;
    private final Double INCH_CENTIMETER = 2.54;
    private final Double GALLON_LITER = 3.7854;
    private final Double POUND_KILOGRAM = 0.4536;

    public void imperialInterface() {
        System.out.println(" _______________________________________________");
        System.out.println("| Choose unit:                                  |");
        System.out.println("| 1: foot to meter                              |");
        System.out.println("| 2: inch to centimeter                         |");
        System.out.println("| 3: gallon to liter                            |");
        System.out.println("| 4: pound to kilogram                          |");
        System.out.println("| 5: degree Fahrenheit to degree Celsius        |");
        System.out.println("|_______________________________________________|");
    }

    public Double footToMeter(Double number) throws LessThanZeroException {

        Double meter;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            meter = number * FOOT_METER;
        }
        return meter;
    }

    public Double inchToCentimeter(Double number) throws LessThanZeroException {

        Double centimeter;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            centimeter = number * INCH_CENTIMETER;
        }
        return centimeter;
    }

    public Double gallonToLiter(Double number) throws LessThanZeroException {

        Double liter;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            liter = number * GALLON_LITER;
        }
        return liter;
    }

    public Double poundToKilogram(Double number) throws LessThanZeroException {

        Double kilogram;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            kilogram = number * POUND_KILOGRAM;
        }
        return kilogram;
    }

    public Double fahrenheitToCelsius(Double number) throws LessThanZeroException {

        Double celsius = (number - 32.0) * (5.0 / 9.0);
        return celsius;
    }
}