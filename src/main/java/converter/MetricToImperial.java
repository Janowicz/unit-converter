package converter;

import exception.LessThanZeroException;

public class MetricToImperial {

    private final Double METER_FOOT = 3.2808;
    private final Double CENTIMETER_INCH = 0.3937;
    private final Double LITER_GALLON = 0.2642;
    private final Double KILOGRAM_POUND = 2.2046;


    public void metricInterface() {
        System.out.println(" _______________________________________________");
        System.out.println("| Choose unit:                                  |");
        System.out.println("| 1: meter to foot                              |");
        System.out.println("| 2: centimeter to inch                         |");
        System.out.println("| 3: liter to gallon                            |");
        System.out.println("| 4: kilogram to pound                          |");
        System.out.println("| 5: degree Celsius to degree Fahrenheit        |");
        System.out.println("|_______________________________________________|");
    }

    public Double meterToFoot(Double number) {

        Double foot;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            foot = number * METER_FOOT;
        }
        return foot;
    }

    public Double centimeterToInch(Double number) {

        Double inch;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            inch = number * CENTIMETER_INCH;
        }
        return inch;

    }

    public Double literToGallon(Double number) {

        Double gallon;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            gallon = number * LITER_GALLON;
        }
        return gallon;
    }

    public Double kilogramToPound(Double number) {

        Double pound;
        if (number < 0) {
            throw new LessThanZeroException("Number is less than 0!");
        } else {
            pound = number * KILOGRAM_POUND;
        }
        return pound;
    }

    public Double celsiusToFahrenheit(Double number) {

        Double fahrenheit = number * 9.0 / 5.0 + 32.0;
        return fahrenheit;
    }

}
