package exception;

public class LessThanZeroException extends RuntimeException{

    public LessThanZeroException(String message) {
        super(message);
    }
}
